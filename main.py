
import numpy as np
import tensorflow as tf
import tensorflow_datasets as tfds
import matplotlib.pyplot as plt
import pandas as pd





#returns function dependent on batch size which will be applied to tf dataset
def prepare_mnist_data(batch_size): 
    def return_fun(mnist):
        #convert int to float
        mnist = mnist.map(lambda img, target: (tf.cast(img, tf.float32), target))
        #flatten the images into vectors
        mnist = mnist.map(lambda img, target: (tf.reshape(img, (-1,)), target))
        #normalize pixel values to [-1,1]
        mnist = mnist.map(lambda img, target: ((img/128.)-1., target))
        #create one-hot targets
        mnist = mnist.map(lambda img, target: (img, tf.one_hot(target, depth=10)))

        #cache this progress in memory
        mnist = mnist.cache()
        #shuffle, batch, prefetch
        mnist = mnist.shuffle(1000)
        mnist = mnist.batch(batch_size)
        mnist = mnist.prefetch(20)
        #return preprocessed dataset
        return mnist
    return return_fun



class mnist_model(tf.keras.Model):
    
    #takes tuple of list of hidden layers and an act_fun and adds all layers to model
    def __init__(self, hidden_layers, act_fun):
        super(mnist_model, self).__init__()
        self.n_layers = len(hidden_layers)  #keep number of layers as attribute for later use
        for n in range(self.n_layers):
            attr_name = f'Layer{n+1}'
            setattr(self, attr_name, tf.keras.layers.Dense(hidden_layers[n], activation=act_fun))
        self.out = tf.keras.layers.Dense(10, activation=tf.nn.softmax)

    @tf.function
    def call(self, inputs):
        layer1 = getattr(self, f'Layer{1}')
        x = layer1(inputs)
        for n in range(1, self.n_layers):  #we skip the first layer, since it gets computed outside the for loop  
            current_layer = getattr(self, f'Layer{n+1}')
            x = current_layer(x)
        x = self.out(x)
        return x    #return prediction



#train model on one minibatch and return training loss
def train_step(model, input, target, loss_function, optimizer):
    # loss_object and optimizer_object are instances of respective tensorflow classes
    with tf.GradientTape() as tape:
        prediction = model.call(input)
        loss = loss_function(target, prediction)
    gradients = tape.gradient(loss, model.trainable_variables)
    optimizer.apply_gradients(zip(gradients, model.trainable_variables))
    return loss


#test model on test data
def test(model, test_data, loss_function):
    # test over complete test data

    test_accuracy_aggregator = []
    test_loss_aggregator = []

    for (input, target) in test_data:
        prediction = model.call(input)
        sample_test_loss = loss_function(target, prediction)
        sample_test_accuracy =  np.argmax(target, axis=1) == np.argmax(prediction, axis=1)
        sample_test_accuracy = np.mean(sample_test_accuracy)
        test_loss_aggregator.append(sample_test_loss.numpy())
        test_accuracy_aggregator.append(np.mean(sample_test_accuracy))

    test_loss = tf.reduce_mean(test_loss_aggregator)
    test_accuracy = tf.reduce_mean(test_accuracy_aggregator)

    return test_loss, test_accuracy


#training loop
def train_model(n_epochs, model, training_data, test_data, loss_function, optimizer):

    #aggregator lists for losses and accuracies per epoch
    train_losses = []
    test_losses = []
    test_accuracies = []

    #test model performance before training
    loss, acc = test(model, test_data, loss_function)
    test_losses.append(loss)
    test_accuracies.append(acc)

    loss, acc = test(model, training_data, loss_function)
    train_losses.append(loss)

    for n in range(n_epochs):
        
        print(f'start epoch {n}')

        #collect losses of each minibatch
        epoch_loss_agg = []
        for input, target in training_data:
            train_loss = train_step(model, input, target, loss_function, optimizer)
            #append loss of this minibatch
            epoch_loss_agg.append(train_loss)

        #append overall training loss of epoch
        train_losses.append(tf.reduce_mean(epoch_loss_agg))

        #compute and append test loss and acc
        test_loss, test_accuracy = test(model, test_data, loss_function)
        test_losses.append(test_loss)
        test_accuracies.append(test_accuracy)


    return train_losses, test_losses, test_accuracies


#compute performance for various combinaions of hyperparameter settings
#we can change batch sizes, number of epchs, learning rate, number of layers and layer sizes, activation function, loss function
def compare_hyperparameters(training_dataset, test_dataset, batch_sizes, n_epochs, learning_rates, momenta, model_configs, activation_functions, loss_functions):

    measurements = []

    for batch_size in batch_sizes: 
        training_data = training_dataset.apply(prepare_mnist_data(batch_size))
        test_data = test_dataset.apply(prepare_mnist_data(batch_size))
        for activation_function in activation_functions:    
            for model_config in model_configs:
                #build model
                current_model = mnist_model(model_config, activation_function)
                for learning_rate in learning_rates:
                    for loss_function in loss_functions:
                        for momentum in momenta:
                            #we now have a specific model with specific hyperparameters
                            optimizer = tf.keras.optimizers.SGD(learning_rate=learning_rate, momentum=momentum)
                            train_losses, test_losses, test_accuracies = train_model(n_epochs, current_model, training_data, test_data, loss_function, optimizer)

                            measurements.append((batch_size, learning_rate, momentum, model_config, activation_function.__name__, loss_function.__class__.__name__, train_losses, test_losses, test_accuracies))
    return measurements



def visualize(measurments):
    n = len(measurments)
    grid_shape = int(np.ceil(np.sqrt(n)))
    i = 0
    j = 0
    for bs, lr, m, mc, af, lf, train_losses, test_losses, test_accuracies in measurments:
        print(i,j)
        plot = plt.subplot2grid((grid_shape, grid_shape), (i,j))
        plot.plot(train_losses, label='training')
        plot.plot(test_losses, label='test')
        plot.plot(test_accuracies, label='test accuracy')
        plot.legend()
        plot.set_title(f'bs={bs}, lr={lr}, m={m}, lrs={mc}, act={af}, loss={lf}', fontsize=9)
        #plot.set_xlabel('Epochs', fontsize=9)
        plot.set_ylabel('Loss/Accuracy')
        plot.set_ylim(0, 1.2)
        j += 1
        if j == grid_shape:
            i += 1
            j = 0
    plt.show()

def as_pandas_df(measurements):
    cols = ["Batch sizes", "Epoch",  "Learning Rates", "Momentum", "Model configurations", "act fun", "loss fun", "train_losses", "test_losses", "test_accuracies"]
    df = pd.DataFrame(columns=cols)
    for bs, lr, m, mc, af, lf, train_losses, test_losses, test_accuracies in measurements:
        for n in range(len(train_losses)):
            df.loc[len(df)] = [bs, n, lr, m, mc, af, lf, train_losses[n].numpy(), test_losses[n].numpy(), test_accuracies[n].numpy()]
    return df

#HERE WE DEFINE OUR TEST VARIABLES
#hidden_layers
(train_ds, test_ds), ds_info = tfds.load('mnist', split = ['train', 'test'], as_supervised = True, with_info = True)
batch_sizes = [64, ]
n_epochs = 6
learning_rates = [0.05,]
momenta = [0, ]
loss_functions = [tf.keras.losses.CategoricalCrossentropy(), ]  
model_configs = [(256, 128, 64, 32, 16), ]
activation_functions = [tf.nn.relu]


param_comparisons = compare_hyperparameters(train_ds, test_ds, batch_sizes, n_epochs, learning_rates, momenta, model_configs, activation_functions, loss_functions)
print(param_comparisons)
visualize(param_comparisons)
mydf = as_pandas_df(param_comparisons)
print(mydf)
mydf.to_csv("layer-configs.csv.csv", sep=";") 



#INTERESTING COMBINATIONS
#1) varying batch size:
#batch_sizes = [32, 128, 512, 1024,  2048, 4096]
#n_epochs = 6
#learning_rates = [0.05,]
#momenta = [0, ]
#loss_functions = [tf.keras.losses.CategoricalCrossentropy(), ]  
#model_configs = [(256, 128, 64, 32, 16)]
#activation_functions = [tf.nn.relu]
# with increasing batch size, the predictions get worse.
# Explanation: less weight adjustments per epoch and therefore also less weight adjustments in total for larger batch sizes
# Advantage: training takes less time


#2)
#batch_sizes = [64, ]
#n_epochs = 6
#learning_rates = [0.05,]
#momenta = [0, ]
#loss_functions = [tf.keras.losses.CategoricalCrossentropy(), ]  
#model_configs = [(256, 128, 64, 32, 16), (16, 32, 64, 128, 256), (50, 50), (50, 50, 50, 50), (100, 100), (100, 100, 100, 100), (500, 500)]
#activation_functions = [tf.nn.relu]
# interesting about these models: the ones with more layers had a less stable learning trajectory
#very close results, but the models with larger layers sizes were slightly better





#print(ds_info)
# HOW MANY IMAGES?
#     total_num_examples=70000,
#    splits={
#        'test': 10000,
#        'train': 60000,
# WHAT'S THE THE IMAGE SHAPE?
#   image': Image(shape=(28, 28, 1), dtype=tf.uint8),

#WHAT RANGE ARE THE PIXEL VALUES IN?
#   We look for the min and max pixel value
#tdata = train_ds.apply(prepare_mnist_data(32))
#max_agg = []
#for picture, _ in test_ds:
#  max_agg.append(np.max(picture.numpy()))
#print(f'max pixel value: {np.max(max_agg)}')
#min_agg = []
#for picture, target in test_ds:
#  min_agg.append(np.min(picture.numpy()))
#print(f'max pixel value: {np.min(min_agg)}')